package com.example.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/")
@Slf4j
public class SampleController {
//    @Autowired
//    private SchoolService schoolService;

    @Resource
    private UserDao userDao;


    private Long count = 0L;


    @ApiOperation(value = "测试")
    @GetMapping("/home")
    String home() {
        return "Hello World!";
    }


//    @ApiOperation(value = "采取mybatis方式查询")
//    @ApiImplicitParam(name = "id", value = "用户id", paramType = "path", required = true)
//    @GetMapping("/schools/maybatis/{id}")
//    public School helloSchool1(@PathVariable("id") int id) {
//        School school = schoolService.getSchoolById(id);
//        System.out.println(school);
//        return school;
//    }
//
//    @ApiOperation(value = "采取mybatis-plus方式查询")
//    @ApiImplicitParam(name = "id", value = "用户id", paramType = "path", required = true)
//    @GetMapping("/schools/maybatis-plus/{id}")
//    public School helloSchool2(@PathVariable("id") int id) {
//        School school = schoolService.getSchoolById(id);
//        System.out.println(school);
//        return school;
//    }
//
//    @ApiOperation(value = "插入数据")
//    @ApiImplicitParam(name = "school", value = "学校信息", paramType = "body", required = true)
//    @PostMapping("/schools")
//    public Boolean insertSchool(@RequestBody School school) {
//        Boolean tag = schoolService.save(school);
//        System.out.println(tag);
//        return tag;
//    }

    @GetMapping("/user")
    public User userInfo(@RequestParam String user) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName, user);
        User result = userDao.selectOne(queryWrapper);
//        log.info("user: {}", result);
        count = count+1;

        Date timea = new Date();
        log.info("time: {} ==============count: {}",timea,count);

        return result;
    }
}