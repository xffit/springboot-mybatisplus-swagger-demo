package com.example.demo.services;


import javafx.util.Pair;

public class TestExtend {
    public static void main(String[] args){
        PairA<Integer> p = new PairA<>(123,456);
        if (p instanceof PairA) {
            System.out.println(p.toString());
        }
        int n = add(p);
        System.out.println(n);
        try {
            PairA<String> s = new PairA(String.class);
            System.out.println(s.getFirst());
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static  int add(PairA<? extends Number> p){
        Number first = p.getFirst();
        Number last = 5;
//        p.setLast(last);
        return first.intValue();
    }
}

class PairA<T> {
    private T first;
    private T last;

    public PairA(Class<T> clazz) throws IllegalAccessException, InstantiationException {
        first = clazz.newInstance();
        last = clazz.newInstance();
    }

    public PairA(T first, T last) {
        this.first = first;
        this.last = last;
    }

    public T getFirst() {
        return first;
    }
    public T getLast() {
        return last;
    }
    public void setFirst(T first) {
        this.first = first;
    }
    public void setLast(T last) {
        this.last = last;
    }
}
