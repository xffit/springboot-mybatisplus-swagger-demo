package com.example.demo.services;

import com.example.demo.entity.User;

public interface UserService {
    User getUser(String user);
}
