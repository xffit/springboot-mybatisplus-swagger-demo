package com.example.demo.services.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    public User getUser(String user){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        queryWrapper.lambda().eq(User::getUserName, user);
        User item = userDao.selectOne(queryWrapper);


        return item;
    }
}
