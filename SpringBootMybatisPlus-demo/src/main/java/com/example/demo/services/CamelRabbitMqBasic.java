package com.example.demo.services;

import com.example.demo.entity.User;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.impl.DefaultCamelContext;

public class CamelRabbitMqBasic {
    public static void main(String[] args) throws Exception {
        // create a CamelContext
        try (CamelContext camel = new DefaultCamelContext()) {

            // add routes which can be inlined as anonymous inner class
            // (to keep all code in a single java file for this basic example)
            camel.addRoutes(createRabbitRoute());

            // start is not blocking
            camel.start();

            // so run for 10 seconds
            Thread.sleep(10_000);
        }
    }

    static RouteBuilder createRabbitRoute() {
        return new RouteBuilder() {
            @Override
            public void configure() {

                from("timer://simple?period=1").setBody().simple("Test message: ${in.headers.CamelTimerCounter}")
                .to("rabbitmq://10.255.144.88:5672/TESTB?queue=testB&username=root&password=cestc@mq&exchangeType=direct&exchangePattern=InOnly&autoDelete=false&durable=true").end();
            }
        };
    }
}
