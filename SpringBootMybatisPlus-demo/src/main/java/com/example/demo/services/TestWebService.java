package com.example.demo.services;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by garfield on 2016/10/12.
 */
public class TestWebService {
    public static void main(String[] args) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        //拼接xml请求,带有请求头

        String soapReq = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <HelloWebXml xmlns=\"http://WebXml.com.cn/\" />\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";
        try {
            String method = "http://www.webxml.com.cn/WebServices/TranslatorWebService.asmx";//比如http://192.177.222.222:8888/services/Service_Name/Function_Name
            PostMethod postMethod = new PostMethod(method);
            byte[] b = soapReq.getBytes("utf-8");
            InputStream is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity re = new InputStreamRequestEntity(is, b.length, "text/xml; charset=utf-8");
            postMethod.setRequestEntity(re);

            HttpClient httpClient = new HttpClient();
            int statusCode = httpClient.executeMethod(postMethod);
            //200说明正常返回数据
            if (statusCode != 200) {
                //internet error
                System.out.println(statusCode);
            }
            String soapResponseData = postMethod.getResponseBodyAsString();
            System.out.println("=============response===================");
            System.out.println(soapResponseData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}